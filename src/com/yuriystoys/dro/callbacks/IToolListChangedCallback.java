package com.yuriystoys.dro.callbacks;


public interface IToolListChangedCallback {
	public void onToolListChangedChanged();
}
