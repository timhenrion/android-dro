package com.yuriystoys.dro.callbacks;

public interface ISelectedToolChanged {
	public void onSelectedToolChanged();
}
