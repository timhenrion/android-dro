/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.core;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import com.yuriystoys.dro.MachineTypes;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.axes.AxisSettings;
import com.yuriystoys.dro.axes.PassThroughAxisReadoutProcessor;
import com.yuriystoys.dro.axes.SummingAxisReadoutProcessor;
import com.yuriystoys.dro.callbacks.IGlobalModeChangedCallback;
import com.yuriystoys.dro.callbacks.IGlobalUnitsChangedCallback;
import com.yuriystoys.dro.callbacks.IModeChangedCallback;
import com.yuriystoys.dro.callbacks.IReadoutCallback;
import com.yuriystoys.dro.callbacks.ISelectedToolChanged;
import com.yuriystoys.dro.settings.PreferenceKeys;

/*
 * Responsible for keeping track of the current (volatile) readout state.
 */
public class Dro implements IReadoutCallback, IModeChangedCallback {

	public final static int SUPPORTED_AXES = Axis.C;

	private boolean metricMode = false;

	public final static double MM_PER_INCH = 25.4;

	public final static double DEFAULT_CPI = 2560;
	public final static double DEFAULT_CPR = 1;

	private int formatCode = 0;

	private int selectedToolId = 0;

	// loaded from the settings.
	private ReadoutFormat inchFormat = getInchFormat(formatCode);
	private ReadoutFormat mmFormat = getMmFormat(formatCode);
	private ReadoutFormat rpmFormat = new ReadoutFormat("##,##0", 3);

	// private final List<Integer> supportedAxes = new ArrayList<Integer>();

	private final AxisSettings[] allAxes = new AxisSettings[SUPPORTED_AXES];
	private final List<AxisSettings> enabledAxes = new ArrayList<AxisSettings>();
	private MachineTypes machineType = MachineTypes.values()[0];

	public Dro() {
		for (int axis = 0; axis < SUPPORTED_AXES; axis++) {
			AxisSettings temp = new AxisSettings(axis, Axis.getAxisType(axis));
			temp.setProcessor(new PassThroughAxisReadoutProcessor(temp));
			temp.registerCallback((IModeChangedCallback) this);
			allAxes[axis] = temp;
		}
	}

	public AxisSettings getAxis(int axis) {
		return allAxes[axis];
	}

	/**
	 * List of all enabled axes
	 * 
	 * @return
	 */
	public List<AxisSettings> getAxes() {
		return enabledAxes;
	}

	public ReadoutFormat getCurrentFormat() {
		return metricMode ? mmFormat : inchFormat;
	}

	public ReadoutFormat getTachometerFormat() {
		return rpmFormat;
	}

	/* ------------------------- */

	/*
	 * Creates a point at the current position (all positions are set in
	 * absolute CPI)
	 */
	public Point getCurrentPoint() {
		return Point.createPoint3D(null, null, getAxis(Axis.X).getAbsolutePosition(), getAxis(Axis.Y)
				.getAbsolutePosition(), getAxis(Axis.Z).getAbsolutePosition(), null);
	}

	// mode and units stuff

	public boolean isInMetricMode() {
		return metricMode;
	}

	public boolean axisEnabled(int axis) {
		return this.enabledAxes.contains(allAxes[axis]);
	}

	public void setZero() {
		for (AxisSettings axis : enabledAxes) {
			axis.setIncrementalOrigin();
			axis.setIncrementalMode();
		}
	}

	public boolean allAxesInAbsoluteMode() {
		for (AxisSettings axis : enabledAxes) {
			if (axis.isInIncrementalMode())
				return false;
		}

		return true;
	}

	public boolean allAxesInIncrementalMode() {
		for (AxisSettings axis : enabledAxes) {
			if (!axis.isInIncrementalMode())
				return false;
		}

		return true;
	}

	public void toggleMetricMode() {

		this.setMetricMode(!metricMode);

	}

	public void setMetricMode(boolean metric) {
		metricMode = metric;

		for (AxisSettings axis : enabledAxes) {
			axis.setMetricUnits(metric);
		}

		raiseOnUnitsChanged();
	}

	// points stuff

	public void setOrigin() {

		// sets all enabled axes to use the current position as workspace origin
		for (AxisSettings axis : enabledAxes) {
			axis.setWorkspaceOrigin();
			axis.setAbsoluteMode();
		}
	}

	public void setPoint(Point point) {
		if (point == null)
			return;

		AxisSettings axis = allAxes[Axis.X];
		axis.setOffset(-point.getX());
		axis.setIncrementalMode();

		axis = allAxes[Axis.Y];
		axis.setOffset(-point.getY());
		axis.setIncrementalMode();

		if (point.getZ() != null) {
			axis = allAxes[Axis.Z];
			axis.setOffset(-point.getZ());
			axis.setIncrementalMode();
		}

	}

	@Override
	public void processReadout(int axis, int position) {

		// we need to update any axis, even if it's not enabled
		allAxes[axis].acceptPosition(position);
	}

	public class Position {
		int axis;

		protected Position(int axis) {
			this.axis = axis;
		}
	}

	public void loadPreferences(SharedPreferences sharedPrefs, Context context) {

		Resources resources = context.getResources();

		// set everything to defaults

		for (AxisSettings axis : allAxes) {
			axis.setCountsPerInch(DEFAULT_CPI);
			axis.setInverted(false);
			axis.setAverage(false);
			axis.resetPosition();
			axis.clearScale();
		}

		int machineType = Integer.parseInt(sharedPrefs.getString(PreferenceKeys.MACHINE_TYPE, "0"));
		this.machineType = MachineTypes.values()[machineType];

		enabledAxes.clear();

		// Load axis format settings
		try {
			formatCode = (int) Integer.parseInt(sharedPrefs.getString(PreferenceKeys.DISPLAY_FORMAT,
					Integer.toString(0)));
		} catch (NumberFormatException ex) {
			formatCode = 0;
		}

		this.inchFormat = getInchFormat(formatCode);
		this.mmFormat = getMmFormat(formatCode);

		int wAxisTarget = Integer.parseInt(sharedPrefs.getString(PreferenceKeys.W_TARGET, Integer.toString(Axis.W)));

		AxisSettings axis = allAxes[Axis.X];

		// there is no setting for X yet, so it's enabled by default
		if (sharedPrefs.getBoolean(PreferenceKeys.X_ENABLE, true)) {
			enabledAxes.add(axis);

			try {
				axis.setCountsPerInch(Double.parseDouble(sharedPrefs.getString(PreferenceKeys.X_TPI,
						Double.toString(DEFAULT_CPI))));
			} catch (NumberFormatException ex) {
				axis.setCountsPerInch(DEFAULT_CPI);
			}

			axis.setEnabled(true);
			axis.setInverted(sharedPrefs.getBoolean(PreferenceKeys.X_INVERT, false));
			axis.setAverage(sharedPrefs.getBoolean(PreferenceKeys.X_FILTER, false));
			axis.setLabel(sharedPrefs.getString(PreferenceKeys.X_LABEL, resources.getString(R.string.x_axis_label)));
		} else {
			axis.setEnabled(false);
		}

		// Y Axis
		axis = allAxes[Axis.Y];

		if (sharedPrefs.getBoolean(PreferenceKeys.Y_ENABLE, true)) {
			enabledAxes.add(axis);

			try {
				axis.setCountsPerInch(Double.parseDouble(sharedPrefs.getString(PreferenceKeys.Y_TPI,
						Double.toString(DEFAULT_CPI))));
			} catch (NumberFormatException ex) {
				axis.setCountsPerInch(DEFAULT_CPI);
			}

			axis.setEnabled(true);
			axis.setInverted(sharedPrefs.getBoolean(PreferenceKeys.Y_INVERT, false));
			axis.setAverage(sharedPrefs.getBoolean(PreferenceKeys.Y_FILTER, false));
			axis.setLabel(sharedPrefs.getString(PreferenceKeys.Y_LABEL, resources.getString(R.string.y_axis_label)));
		} else {
			axis.setEnabled(false);
		}

		// Y Axis
		axis = allAxes[Axis.Z];

		if (sharedPrefs.getBoolean(PreferenceKeys.Z_ENABLE, true)) {
			enabledAxes.add(axis);

			try {
				axis.setCountsPerInch(Double.parseDouble(sharedPrefs.getString(PreferenceKeys.Z_TPI,
						Double.toString(DEFAULT_CPI))));
			} catch (NumberFormatException ex) {
				axis.setCountsPerInch(DEFAULT_CPI);
			}

			axis.setEnabled(true);
			axis.setInverted(sharedPrefs.getBoolean(PreferenceKeys.Z_INVERT, false));
			axis.setAverage(sharedPrefs.getBoolean(PreferenceKeys.Z_FILTER, false));
			axis.setLabel(sharedPrefs.getString(PreferenceKeys.Z_LABEL, resources.getString(R.string.z_axis_label)));
		} else {
			axis.setEnabled(false);
		}

		// special handling for W axis
		axis = allAxes[Axis.W];

		if (sharedPrefs.getBoolean(PreferenceKeys.W_ENABLE, false)) {

			axis.setInverted(sharedPrefs.getBoolean(PreferenceKeys.W_INVERT, false));
			axis.setAverage(sharedPrefs.getBoolean(PreferenceKeys.W_FILTER, false));
			axis.setLabel(sharedPrefs.getString(PreferenceKeys.W_LABEL, resources.getString(R.string.w_axis_label)));

			try {
				axis.setCountsPerInch(Double.parseDouble(sharedPrefs.getString(PreferenceKeys.W_TPI,
						Double.toString(DEFAULT_CPI))));
			} catch (NumberFormatException ex) {
				axis.setCountsPerInch(DEFAULT_CPI);
			}

			if (wAxisTarget == Axis.W) {
				enabledAxes.add(axis);
				axis.setProcessor(new PassThroughAxisReadoutProcessor(axis));
				axis.setEnabled(true);
			} else {
				// the axis needs to be fully setup before adding the summing
				// processor
				axis.setProcessor(new SummingAxisReadoutProcessor(axis, allAxes[wAxisTarget]));
				axis.setEnabled(false);
			}
		} else {
			axis.setEnabled(false);
		}

		axis = allAxes[Axis.T];
		if (sharedPrefs.getBoolean(PreferenceKeys.TACH_ENABLE, false)) {
			enabledAxes.add(allAxes[Axis.T]);

			try {
				axis.setCountsPerInch(Double.parseDouble(sharedPrefs.getString(PreferenceKeys.TACH_PPR,
						Double.toString(DEFAULT_CPR))));
			} catch (NumberFormatException ex) {
				axis.setCountsPerInch(DEFAULT_CPR);
			}

			axis.setEnabled(true);
			axis.setInverted(sharedPrefs.getBoolean(PreferenceKeys.TACH_INVERT, false));
			axis.setAverage(sharedPrefs.getBoolean(PreferenceKeys.TACH_FILTER, false));
			axis.setLabel(sharedPrefs.getString(PreferenceKeys.TACH_LABEL, resources.getString(R.string.tach_label)));
		} else {
			axis.setEnabled(false);
		}
	}

	private ReadoutFormat getInchFormat(int code) {
		switch (code) {
		case 0:
			return (new ReadoutFormat("#0.000", 3));
		case 1:
			return (new ReadoutFormat("#0.0000", 4));
		case 2:
			return (new ReadoutFormat("##0.000", 3));
		case 3:
			return (new ReadoutFormat("##0.0000", 4));
		default:
			throw new IllegalArgumentException("Unexpect inch format code");
		}

	}

	private ReadoutFormat getMmFormat(int code) {
		switch (code) {
		case 0:
			return (new ReadoutFormat("##0.00", 2));
		case 1:
			return (new ReadoutFormat("##0.000", 3));
		case 2:
			return (new ReadoutFormat("###0.00", 2));
		case 3:
			return (new ReadoutFormat("###0.000", 3));
		default:
			throw new IllegalArgumentException("Unexpect inch format code");
		}
	}

	public class ReadoutFormat {

		private final DecimalFormat format;
		private final double factor;

		public ReadoutFormat(String pattern, int numOfDigits) {
			format = new DecimalFormat(pattern);
			factor = Math.pow(10, numOfDigits);
		}

		public String format(double value) {
			value = Math.round(value * factor) / factor;
			return format.format(value);
		}
	}

	/* callbacks */

	private final List<IGlobalModeChangedCallback> modeChangedCallbacks = new ArrayList<IGlobalModeChangedCallback>();
	private final List<IGlobalUnitsChangedCallback> unitsChangedCallbacks = new ArrayList<IGlobalUnitsChangedCallback>();
	private final List<ISelectedToolChanged> toolChangedCallbacks = new ArrayList<ISelectedToolChanged>();

	private void raiseOnModeChanged() {
		for (IGlobalModeChangedCallback callback : modeChangedCallbacks) {
			callback.onModeChanged(this);
		}
	}

	private void raiseOnUnitsChanged() {
		for (IGlobalUnitsChangedCallback callback : unitsChangedCallbacks) {
			callback.onUnitsChanged(this);
		}
	}
	
	private void raiseOnToolChanged() {
		for (ISelectedToolChanged callback : toolChangedCallbacks) {
			callback.onSelectedToolChanged();
		}
	}

	public void registerCallback(IGlobalModeChangedCallback callback) {
		if (!modeChangedCallbacks.contains(callback)) {
			modeChangedCallbacks.add(callback);
		} else {
			throw new IllegalStateException("This callback is already registered.");
		}
	}

	public void registerCallback(IGlobalUnitsChangedCallback callback) {
		if (!unitsChangedCallbacks.contains(callback)) {
			unitsChangedCallbacks.add(callback);
		} else {
			throw new IllegalStateException("This callback is already registered.");
		}
	}
	
	public void registerCallback(ISelectedToolChanged callback) {
		if (!toolChangedCallbacks.contains(callback)) {
			toolChangedCallbacks.add(callback);
		} else {
			throw new IllegalStateException("This callback is already registered.");
		}
	}

	public void removeCallback(IGlobalModeChangedCallback callback) {
		if (modeChangedCallbacks.contains(callback))
			modeChangedCallbacks.remove(callback);
		else
			throw new IllegalStateException("Attempting to remove a callback that is not registered");
	}

	public void removeCallback(IGlobalUnitsChangedCallback callback) {
		if (unitsChangedCallbacks.contains(callback))
			unitsChangedCallbacks.remove(callback);
		else
			throw new IllegalStateException("Attempting to remove a callback that is not registered");
	}

	public void removeCallback(ISelectedToolChanged callback) {
		if (toolChangedCallbacks.contains(callback))
			toolChangedCallbacks.remove(callback);
		else
			throw new IllegalStateException("Attempting to remove a callback that is not registered");
	}

	@Override
	public void onModeChanged(AxisSettings sender) {
		raiseOnModeChanged();
	}

	public MachineTypes getMachineType() {
		return machineType;
	}

	public void setSelectedToolId(int id) {
		selectedToolId = id;
		raiseOnToolChanged();
	}

	public void clearSelectedToolId() {
		selectedToolId = -1;
		raiseOnToolChanged();
	}

	public int getSelectedToolId() {
		return this.selectedToolId;
	}
}
