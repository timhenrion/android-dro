/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.core;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;

import com.yuriystoys.dro.callbacks.IPointListChangedCallback;
import com.yuriystoys.dro.data.Repository;
import com.yuriystoys.dro.settings.PreferenceKeys;

/**
 * 
 * @author Yuriy Krushelnytskiy Workspace is the main container for the
 *         session-specific data, such as offsets, modes,etc.
 * 
 */
public class Workspace {



	// settings
	private final List<IPointListChangedCallback> pointListChangedCallbacks = new ArrayList<IPointListChangedCallback>();


	private Integer id; // id can be null as long as the workspace isn't saved

	private Integer bankId = -1;
	private Integer selectedPointId = -1;
	private String name;
	private String description; // Description can be null (it's optional)

	private Date createdOn;
	private Date updatedOn;

	private Context context;

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public int getBankId()
	{
		return bankId;
	}
	
	public void setBankId(int value)
	{
		bankId = value;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	private Workspace() {
	}

	public Workspace(Context context, Cursor c) throws ParseException {
		super();
		this.context = context;
		this.id = c.getInt(c.getColumnIndex(Repository.WORKSPACE_ID));
		this.bankId = c.getInt(c.getColumnIndex(Repository.WORKSPACE_PREFERENCE_BANK_ID));
		this.selectedPointId = -1;
		this.name = c.getString(c.getColumnIndex(Repository.WORKSPACE_NAME));

		if (!c.isNull(c.getColumnIndex(Repository.WORKSPACE_DESCRIPTION)))
			this.description = c.getString(c.getColumnIndex(Repository.WORKSPACE_DESCRIPTION));

		this.createdOn = Repository.iso8601Format.parse(c.getString(c.getColumnIndex(Repository.WORKSPACE_CREATED_ON)));
		this.updatedOn = Repository.iso8601Format.parse(c.getString(c.getColumnIndex(Repository.WORKSPACE_UPDATED_ON)));
	}

	/**
	 * Creates a new workspace using the applicatoin settings
	 * 
	 * @param preferences
	 * @return
	 */
	public static Workspace create(Context context) {
		Workspace temp = new Workspace();

		temp.context = context;
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());				
		int bId = Integer.parseInt(prefs.getString(PreferenceKeys.PREFERENCE_BANK, "0"));
		
		temp.bankId = bId;

		//SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

		return temp;
	}

	public static Workspace getById(int workspaceId, Context context) {
		Repository repo = Repository.open(context);
		Workspace temp = repo.getWorkspace(workspaceId);

		if (temp != null) // this is a legitimate case.
			temp.context = context;

		return temp;
	}

	public static Workspace getLastUsed(Context context) {
		return Workspace
				.getById(
						PreferenceManager.getDefaultSharedPreferences(context).getInt(PreferenceKeys.WORKSPACE_ID, -1),
						context);
	}

	

	//public void quickAddPoint3D() {

		//Point point = Point.createPoint3D(this.id, null, null, getCurrentCPI(Axis.X), getCurrentCPI(Axis.Y),getCurrentCPI(Axis.Z), null);
		//addPoint(point);
	//}

	public void addPoint(Point point) {
		Repository repo = null;

		try {
			repo = Repository.open(context);

			if (this.id == null) {
				// this is a new (unsaved workspace)
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());				
				this.bankId = Integer.parseInt(prefs.getString(PreferenceKeys.PREFERENCE_BANK, "0"));
				this.id = repo.insertWorkspace(this);
				setLastUsed();
			}

			repo.insertPoint(this.id, point);
		} finally {
			repo.close();
		}

		for (IPointListChangedCallback callback : pointListChangedCallbacks)
			callback.onPointListChanged(this);
	}
	
	public void setSelectedPointId(int id)
	{
		selectedPointId = id;
	}
	
	public void clearSelectedPointId()
	{
		selectedPointId = -1;
		refresh();
	}
	
	public int getSelectedPointId()
	{
		return selectedPointId;
	}

	public void hidePoint(int pointId) {
		Repository repo = null;

		try {
			repo = Repository.open(context);

			repo.hidePoint(pointId);
		} finally {
			repo.close();
		}

		for (IPointListChangedCallback callback : pointListChangedCallbacks)
			callback.onPointListChanged(this);
	}

	public List<Point> getPoints() {
		Repository repo = null;

		try {
			repo = Repository.open(context);
			return repo.getPoints(this.id);
		} finally {
			repo.close();
		}
	}

	private void setLastUsed() {
		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
		editor.putInt(PreferenceKeys.WORKSPACE_ID, this.id);
		editor.commit();
	}
	
	public void registerCallback(IPointListChangedCallback callback) {
		if (!pointListChangedCallbacks.contains(callback)) {
			pointListChangedCallbacks.add(callback);
		} else {
			throw new IllegalStateException("This callback is already registered.");
		}
	}
	
	public void removeCallback(IPointListChangedCallback callback) {
		if (pointListChangedCallbacks.contains(callback))
			pointListChangedCallbacks.remove(callback);
		else
			throw new IllegalStateException("Attempting to remove a callback that is not registered");
	}
	
	public void refresh()
	{
		for (IPointListChangedCallback callback : pointListChangedCallbacks)
			callback.onPointListChanged(this);
	}

}
