/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.core;

import java.util.ArrayList;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.yuriystoys.dro.callbacks.IConnectionStateChangedCallback;
import com.yuriystoys.dro.callbacks.IReadoutCallback;
import com.yuriystoys.dro.callbacks.IToastCallback;

public abstract class DroDriver {

	public abstract void connect(String deviceName);

	public abstract void disconnect();

	public abstract void reconnect();

	public abstract int getState();

	// Intent request codes
	public static final int REQUEST_ENABLE_BT = 1;

	protected final static int SUPPORTED_AXES = 4;

	// connection "watchdog"
	protected static Handler timeoutHandler = null;

	protected static final int WATCHDOG_REFRESH = 1;
	protected static final int WATCHDOG_DISCONNECT = 2;
	protected static final int WATCHDOG_DISCONNECT_INACTIVE = 3;

	protected static final int CYCLES_BEFORE_DISCONNECT = 60;
	protected static final int INACTIVE_CYCLES_BEFORE_DISCONNECT = 2;

	// milliseconds to next refresh
	protected static final int WATCHDOG_REFRESH_INTERVAL = 1000;

	protected static boolean visited = false;
	protected static int disconnectCycles = 0;
	protected static int inactiveCycles = 0;

	private Boolean keepConnection = false;
	protected static volatile int connectionState = ConnectionState.UNKNOWN;

	protected static final ArrayList<IReadoutCallback> readoutCallbacks = new ArrayList<IReadoutCallback>();
	protected static final ArrayList<IConnectionStateChangedCallback> stateCallbacks = new ArrayList<IConnectionStateChangedCallback>();
	protected static final ArrayList<IToastCallback> toastCallbacks = new ArrayList<IToastCallback>();

	// Ctors

	public DroDriver(Context context) {

		timeoutHandler = new Handler(new Handler.Callback() {
			@Override
			public boolean handleMessage(Message msg) {

				switch (msg.what) {
				case WATCHDOG_REFRESH: {
					if (visited) {
						if (connectionState == ConnectionState.CONNECTION_LOST) {
							// reset any "connection lost" stuff
							disconnectCycles = 0;
							processConnectionStatus(ConnectionState.CONNECTED);
						} else {
							timeoutHandler.sendEmptyMessageDelayed(WATCHDOG_REFRESH, WATCHDOG_REFRESH_INTERVAL);
						}
					} else {
						if (disconnectCycles++ == 0) {
							processConnectionStatus(ConnectionState.CONNECTION_LOST);
							timeoutHandler.sendEmptyMessageDelayed(WATCHDOG_REFRESH, WATCHDOG_REFRESH_INTERVAL);
						} else if (disconnectCycles++ < CYCLES_BEFORE_DISCONNECT) {
							timeoutHandler.sendEmptyMessageDelayed(WATCHDOG_REFRESH, WATCHDOG_REFRESH_INTERVAL);
						} else {
							timeoutHandler.sendEmptyMessageDelayed(WATCHDOG_DISCONNECT, WATCHDOG_REFRESH_INTERVAL);
						}
					}
					break;
				}
				case WATCHDOG_DISCONNECT: {
					disconnect();
					break;
				}
				case WATCHDOG_DISCONNECT_INACTIVE: {
					if (inactiveCycles++ < INACTIVE_CYCLES_BEFORE_DISCONNECT)
						// this is an explicit message, so we do as told
						disconnect();
					break;
				}
				}

				visited = false;
				return false;
			}
		});
	}

	public static void processConnectionStatus(int status) {

		connectionState = status;

		// stop the watchdog
		timeoutHandler.removeMessages(WATCHDOG_REFRESH);
		timeoutHandler.removeMessages(WATCHDOG_DISCONNECT_INACTIVE);
		timeoutHandler.removeMessages(WATCHDOG_DISCONNECT);

		if (status == ConnectionState.CONNECTED) {
			// if we are connected, start connection watchdog

			timeoutHandler.sendEmptyMessageDelayed(WATCHDOG_REFRESH, WATCHDOG_REFRESH_INTERVAL);

		}

		for (IConnectionStateChangedCallback callback : stateCallbacks)
			callback.onConnectionStateChanged(status);

	}

	// Methods to add callbacks

	public void registerCallback(IReadoutCallback callback) {
		if (!readoutCallbacks.contains(callback)) {
			readoutCallbacks.add(callback);
		} else {
			throw new IllegalStateException("This callback is already registered.");
		}

		if (connectionState != ConnectionState.CONNECTED && connectionState != ConnectionState.CONNECTING)
			reconnect();

		timeoutHandler.removeMessages(WATCHDOG_DISCONNECT_INACTIVE);
	}

	public void registerCallback(IConnectionStateChangedCallback callback) {
		if (!stateCallbacks.contains(callback)) {
			stateCallbacks.add(callback);
		} else {
			throw new IllegalStateException("This callback is already registered.");
		}
	}

	public void registerCallback(IToastCallback callback) {
		if (!toastCallbacks.contains(callback)) {
			toastCallbacks.add(callback);
		} else {
			throw new IllegalStateException("This callback is already registered.");
		}
	}

	// Methods to remove callbacks

	public void removeCallback(IReadoutCallback callback) {
		if (readoutCallbacks.contains(callback)) {
			readoutCallbacks.remove(callback);

			if (readoutCallbacks.isEmpty() && !keepConnection)
				// if no one cancels the timeout, disconnect
				timeoutHandler.sendEmptyMessageDelayed(WATCHDOG_DISCONNECT_INACTIVE, WATCHDOG_REFRESH_INTERVAL);

			keepConnection = false; // reset "keepConnection"
		} else
			throw new IllegalStateException("Attempting to remove a callback that is not registered");
	}

	public void removeCallback(IConnectionStateChangedCallback callback) {
		if (stateCallbacks.contains(callback))
			stateCallbacks.remove(callback);
		else
			throw new IllegalStateException("Attempting to remove a callback that is not registered");
	}

	public void removeCallback(IToastCallback callback) {
		if (toastCallbacks.contains(callback))
			toastCallbacks.remove(callback);
		else
			throw new IllegalStateException("Attempting to remove a callback that is not registered");
	}

	
	/* Handler (the outside world should not see it.) */
	/**
	 * Hanlder's role is to receive messages from the BT service and route them
	 * to an appropriate callbacks.
	 */
	protected static class ReadoutHandler extends Handler {

		// private final String TAG = DroDriver2.class.getSimpleName();
		// private final boolean D = true;

		@Override
		/**
		 * Handles messages from the communication service
		 * and forwards them to the appropriate method
		 */
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case ServiceMessage.STATE_CHANGE:

				processConnectionStatus(msg.arg1);

				break;
			case ServiceMessage.POSITION_READ:
				// position received
				visited = true;

				int axis = msg.arg1;
				int position = msg.arg2;
				
				//Log.i("Position", Integer.toString(axis));

				for (IReadoutCallback l : readoutCallbacks)
					l.processReadout(axis, position);

				break;
			case ServiceMessage.DEVICE_CONNECTED:

				for (IToastCallback callback : toastCallbacks)
					callback.MakeToast("Connected to " + msg.getData().getString(BluetoothService.DEVICE_NAME));

				break;
			case ServiceMessage.TOAST:
				for (IToastCallback callback : toastCallbacks)
					callback.MakeToast(msg.getData().getString(BluetoothService.TOAST));

				break;
			}
		}
	}
}
