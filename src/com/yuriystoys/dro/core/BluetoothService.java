/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.core;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.yuriystoys.dro.callbacks.IReadoutCallback;

/**
 * This code is based on the BluetoothChatService that can be found in the
 * "samples" folder of the SDK.
 */

public class BluetoothService {
	// Debugging
	private static final String TAG = BluetoothService.class.getSimpleName();
	private static final boolean D = true;
	
	// Key names received from the Handler
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// Member fields
	private final BluetoothAdapter adapter;
	private final Handler handler;
	private ConnectThread connectThread;
	private ConnectedThread connectedThread;
	private int mState;

	/**
	 * Constructor. Prepares a new BluetoothChat session.
	 * 
	 * @param context
	 *            The UI Activity Context
	 * @param handler
	 *            A Handler to send messages back to the UI Activity
	 */
	public BluetoothService(Handler handler) {
		adapter = BluetoothAdapter.getDefaultAdapter();
		mState = ConnectionState.DISCONNECTED;
		this.handler = handler;
		Log.d(TAG, "Constuctor called");
	}
	
	/**
	 * Set the current state of the chat connection
	 * 
	 * @param state
	 *            An integer defining the current connection state
	 */
	private synchronized void setState(int state) {
		if (D)
			Log.d(TAG, "setState() " + mState + " -> " + state);
		mState = state;

		// Give the new state to the Handler so the UI Activity can update
		handler.obtainMessage(ServiceMessage.STATE_CHANGE, state, -1).sendToTarget();
	}

	/**
	 * Return the current connection state.
	 */
	public synchronized int getState() {
		return mState;
	}

	/**
	 * Start the ConnectThread to initiate a connection to a remote device.
	 * 
	 * @param device
	 *            The BluetoothDevice to connect
	 * @param secure
	 *            Socket Security type - Secure (true) , Insecure (false)
	 */
	public synchronized void connect(BluetoothDevice device) {
		if (D)
			Log.d(TAG, "connect to: " + device);

		// Cancel any thread currently running a connection
		if (connectThread != null) {
			connectThread.cancel();
			connectThread = null;
		}
		
		if (connectedThread != null) {
			connectedThread.cancel();
			connectedThread = null;
		}

		// Start the thread to connect with the given device
		connectThread = new ConnectThread(device);
		connectThread.start();
		setState(ConnectionState.CONNECTING);
	}

	/**
	 * Start the ConnectedThread to begin managing a Bluetooth connection
	 * 
	 * @param socket
	 *            The BluetoothSocket on which the connection was made
	 * @param device
	 *            The BluetoothDevice that has been connected
	 */
	public synchronized void connected(BluetoothSocket socket, BluetoothDevice device, final String socketType) {
		if (D)
			Log.d(TAG, "connected, Socket Type:" + socketType);

		// Cancel the thread that completed the connection
		if (connectThread != null) {
			connectThread.cancel();
			connectThread = null;
		}

		// Cancel any thread currently running a connection
		if (connectedThread != null) {
			connectedThread.cancel();
			connectedThread = null;
		}

		// Start the thread to manage the connection and perform transmissions
		connectedThread = new ConnectedThread(socket, socketType);
		connectedThread.start();

		// Send the name of the connected device back to the UI Activity
		Message msg = handler.obtainMessage(ServiceMessage.DEVICE_CONNECTED);
		Bundle bundle = new Bundle();
		bundle.putString(DEVICE_NAME, device.getName());
		msg.setData(bundle);
		handler.sendMessage(msg);

		setState(ConnectionState.CONNECTED);
	}

	/**
	 * Stop all threads
	 */
	public synchronized void stop() {
		Log.d(TAG, "stop");

		if (connectThread != null) {
			connectThread.cancel();
			connectThread = null;
		}

		if (connectedThread != null) {
			connectedThread.cancel();
			connectedThread = null;
		}

		setState(ConnectionState.DISCONNECTED);
		
	}

	/**
	 * Indicate that the connection attempt failed and notify the UI Activity.
	 */
	private void connectionFailed() {
		// Send a failure message back to the Activity
		Message msg = handler.obtainMessage(ServiceMessage.TOAST);
		Bundle bundle = new Bundle();
		bundle.putString(TOAST, "Unable to connect device");
		msg.setData(bundle);
		handler.sendMessage(msg);
		
		setState(ConnectionState.DISCONNECTED);
	}

	/**
	 * Indicate that the connection was lost and notify the UI Activity.
	 */
	private void connectionLost() {
		// Send a failure message back to the Activity
		Message msg = handler.obtainMessage(ServiceMessage.TOAST);
		Bundle bundle = new Bundle();
		bundle.putString(TOAST, "Device connection was lost");
		msg.setData(bundle);
		handler.sendMessage(msg);
		
		setState(ConnectionState.DISCONNECTED);
	}

	/**
	 * This thread runs while attempting to make an outgoing connection with a
	 * device. It runs straight through; the connection either succeeds or
	 * fails.
	 */
	private class ConnectThread extends Thread {
		private final BluetoothSocket _socket;
		private final BluetoothDevice mmDevice;
		private String mSocketType;

		public ConnectThread(BluetoothDevice device) {
			mmDevice = device;
			BluetoothSocket tmp = null;
			mSocketType = "Insecure";

			// Get a BluetoothSocket for a connection with the
			// given BluetoothDevice
			try {
				Method m;
				m = device.getClass().getMethod("createRfcommSocket", new Class[] { int.class });
				tmp = (BluetoothSocket) m.invoke(device, 1);

			} catch (Throwable e) {
				Log.e(TAG, "Socket Type: " + mSocketType + "create() failed", e);
			}
			_socket = tmp;
		}

		public void run() {
			Log.i(TAG, "BEGIN mConnectThread SocketType:" + mSocketType);
			setName("ConnectThread" + mSocketType);

			// Always cancel discovery because it will slow down a connection
			adapter.cancelDiscovery();

			try {
				// This is a blocking call and will only return on a
				// successful connection or an exception
				Log.d(TAG, "attempting to connect to " + mSocketType + " socket");
				_socket.connect();
			} catch (IOException e) {
				// Close the socket
				try {
					_socket.close();
				} catch (IOException e2) {
					Log.e(TAG, "unable to close() " + mSocketType + " socket during connection failure", e2);
				}
				connectionFailed();
				return;
			}

			// Reset the ConnectThread because we're done
			synchronized (BluetoothService.this) {
				connectThread = null;
			}

			// Start the connected thread
			connected(_socket, mmDevice, mSocketType);
		}

		public void cancel() {
			try {
				_socket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect " + mSocketType + " socket failed", e);
			}
		}
	}

	/**
	 * This thread runs during a connection with a remote device. It handles all
	 * incoming and outgoing transmissions.
	 */
	private class ConnectedThread extends Thread implements IReadoutCallback {
		private final BluetoothSocket mmSocket;
		private final InputStream mmInStream;
		//private final OutputStream mmOutStream;
		private final ReadoutParser parser;
		
		public ConnectedThread(BluetoothSocket socket, String socketType) {
			Log.d(TAG, "create ConnectedThread: " + socketType);
			mmSocket = socket;
			InputStream tmpIn = null;
			//OutputStream tmpOut = null;

			// Get the BluetoothSocket input and output streams
			try {
				tmpIn = socket.getInputStream();
				//tmpOut = socket.getOutputStream();
			} catch (IOException e) {
				Log.e(TAG, "temp sockets not created", e);
			}

			mmInStream = tmpIn;
			//mmOutStream = tmpOut;

			parser = new ReadoutParser(this);
		}

		public void run() {

			byte[] readChar = new byte[1];
			int offset = 0;

			// Keep listening to the InputStream while connected
			while (true) {
				try {

					// Try to read a single character from the input stream
					offset = mmInStream.read(readChar, 0, 1);

					// if something was read, send it to the state machine
					if (offset > 0) {
						// parser will process the input and [if appropriate]
						// call the listener's callback method
						parser.Accept((char) readChar[0]);
					}

				} catch (IOException e) {
					Log.e(TAG, "disconnected", e);
					connectionLost();
					break;
				}
			}
		}

		public void cancel() {
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed", e);
			}
		}
		

		@Override
		public void processReadout(int axis, int position) {
			handler.obtainMessage(ServiceMessage.POSITION_READ, axis, position, 0).sendToTarget();
		}
	}
}
