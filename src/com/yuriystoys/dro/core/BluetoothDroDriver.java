/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.core;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.util.Log;

public class BluetoothDroDriver extends DroDriver {

	private static final String TAG = BluetoothDroDriver.class.getSimpleName();

	private ReadoutHandler handler = null;
	private BluetoothAdapter adapter = null;
	private BluetoothService service = null;

	private BluetoothDevice btDevice = null;

	public BluetoothDroDriver(Context context) {

		super(context);

		adapter = BluetoothAdapter.getDefaultAdapter();
		handler = new ReadoutHandler();
		service = new BluetoothService(handler);

	}

	public void disconnect() {

		Log.d(TAG, "Dsiconnecting");
		if (service == null)
			throw new IllegalStateException("The service is not initilaized. Call connect() first.");

		timeoutHandler.removeMessages(WATCHDOG_REFRESH);
		timeoutHandler.removeMessages(WATCHDOG_DISCONNECT_INACTIVE);
		timeoutHandler.removeMessages(WATCHDOG_DISCONNECT);

		service.stop();
	}

	public void reconnect() {

		synchronized (this) {

			int state = getState();
			Log.d(TAG, "Reconnecting");

			if (service == null)
				throw new IllegalStateException("The service is not initilaized. Call connect() first.");

			if (state != ConnectionState.CONNECTED && state != ConnectionState.CONNECTING) {
				if (btDevice != null)
					service.connect(btDevice);
			} else {
				Log.d(TAG, "NOOP:Already connected or connecting.");
			}

			// start watching the connection
			timeoutHandler.removeMessages(WATCHDOG_REFRESH);

		}
	}
	
	public void setLastUsedDevice(String deviceName)
	{
		for (BluetoothDevice d : adapter.getBondedDevices()) {

			if (d.getName().equals(deviceName)) {
				this.btDevice = d;
				return;
			}
		}

		throw new IllegalStateException("Device not found");
	}

	public void connect(String deviceName) {

		synchronized (this) {

			int state = getState();
			Log.d(TAG, "Connecting");

			if (service == null)
				throw new IllegalStateException("The service is not initilaized. Call connect() first.");

			if (state != ConnectionState.CONNECTED && state != ConnectionState.CONNECTING) {

				for (BluetoothDevice d : adapter.getBondedDevices()) {

					if (d.getName().equals(deviceName)) {
						service.connect(d);
						this.btDevice = d;
						return;
					}
				}

				throw new IllegalStateException("Device not found");

			} else {
				Log.d(TAG, "NOOP:Already connected or connecting.");
			}

		}
	}

	public String[] getPairedDeviceNames() {
		String[] names = new String[adapter.getBondedDevices().size()];
		int index = 0;

		for (BluetoothDevice d : adapter.getBondedDevices()) {
			names[index++] = d.getName();
		}

		return names;
	}

	/**
	 * Gets the state of the driver
	 * 
	 * @return
	 */
	public synchronized int getState() {
		// If the adapter is null, then BlueTooth is not supported
		if (adapter == null)
			return ConnectionState.UNSUPPORTED;
		else if (!adapter.isEnabled())
			return ConnectionState.DISABLED;
		else if (service == null)
			return ConnectionState.NO_SERVICE;
		else {
			return connectionState;
		}
	}

	public String getCurrentDeviceName() {
		return btDevice != null ? btDevice.getName() : "";
	}

}
