/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.core;

import java.io.IOException;

import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.yuriystoys.dro.callbacks.IReadoutCallback;

public class UsbService {
	// Debugging
	private static final String TAG = UsbService.class.getSimpleName();
	private static final boolean D = true;

	private final Handler handler;
	private ConnectThread connectThread;
	private ConnectedThread connectedThread;
	private int mState;
	private final int baudRate;
	private final UsbManager manager;

	// Message types sent from the BluetoothChatService Handler

	// Key names received from the Handler
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	/**
	 * Constructor. Prepares a new BluetoothChat session.
	 * 
	 * @param context
	 *            The UI Activity Context
	 * @param handler
	 *            A Handler to send messages back to the UI Activity
	 */
	public UsbService(UsbManager manager, Handler handler, int baudRate) {

		this.baudRate = baudRate;
		this.handler = handler;
		this.manager = manager;
	}

	/**
	 * Set the current state of the chat connection
	 * 
	 * @param state
	 *            An integer defining the current connection state
	 */
	private synchronized void setState(int state) {
		if (D)
			Log.d(TAG, "setState() " + mState + " -> " + state);
		mState = state;

		// Give the new state to the Handler so the UI Activity can update
		handler.obtainMessage(ServiceMessage.STATE_CHANGE, state, -1).sendToTarget();
	}

	/**
	 * Return the current connection state.
	 */
	public synchronized int getState() {
		return mState;
	}

	/**
	 * Start the ConnectThread to initiate a connection to a remote device.
	 * 
	 * @param device
	 *            The BluetoothDevice to connect
	 * @param secure
	 *            Socket Security type - Secure (true) , Insecure (false)
	 */
	public synchronized void connect() {
		if (D)
			Log.d(TAG, "connect to USB device");

		// Cancel any thread currently running a connection
		if (connectedThread != null) {
			connectedThread.cancel();
			connectedThread = null;
		}

		// Start the thread to connect with the given device
		connectThread = new ConnectThread(UsbSerialProber.acquire(manager));
		connectThread.start();
		setState(ConnectionState.CONNECTING);
	}

	public synchronized void connected(UsbSerialDriver usbDriver) {
		if (D)
			Log.d(TAG, "connected to USB device");

		// Cancel the thread that completed the connection
		if (connectThread != null) {
			connectThread.cancel();
			connectThread = null;
		}

		// Cancel any thread currently running a connection
		if (connectedThread != null) {
			connectedThread.cancel();
			connectedThread = null;
		}

		// Start the thread to manage the connection and perform transmissions
		connectedThread = new ConnectedThread(usbDriver, baudRate);
		connectedThread.start();

		// Send the name of the connected device back to the UI Activity
		Message msg = handler.obtainMessage(ServiceMessage.DEVICE_CONNECTED);
		Bundle bundle = new Bundle();
		bundle.putString(DEVICE_NAME, "USB");
		msg.setData(bundle);
		handler.sendMessage(msg);

		setState(ConnectionState.CONNECTED);
	}

	/**
	 * Stop all threads
	 */
	public synchronized void stop() {
		if (D)
			Log.d(TAG, "stop");

		if (connectThread != null) {
			connectThread.cancel();
			connectThread = null;
		}

		if (connectedThread != null) {
			connectedThread.cancel();
			connectedThread = null;
		}

		setState(ConnectionState.DISCONNECTED);
	}

	/**
	 * Indicate that the connection attempt failed and notify the UI Activity.
	 */
	private void connectionFailed() {
		// Send a failure message back to the Activity
		Message msg = handler.obtainMessage(ServiceMessage.TOAST);
		Bundle bundle = new Bundle();
		bundle.putString(TOAST, "Unable to connect to USB device");
		msg.setData(bundle);
		handler.sendMessage(msg);

		setState(ConnectionState.DISCONNECTED);
	}

	/**
	 * Indicate that the connection was lost and notify the UI Activity.
	 */
	private void connectionLost() {
		// Send a failure message back to the Activity
		Message msg = handler.obtainMessage(ServiceMessage.TOAST);
		Bundle bundle = new Bundle();
		bundle.putString(TOAST, "Device connection was lost");
		msg.setData(bundle);
		handler.sendMessage(msg);

		setState(ConnectionState.DISCONNECTED);
	}

	/**
	 * This thread runs while attempting to make an outgoing connection with a
	 * device. It runs straight through; the connection either succeeds or
	 * fails.
	 */
	private class ConnectThread extends Thread {

		private UsbSerialDriver driver;

		public ConnectThread(UsbSerialDriver driver) {
			this.driver = driver;
		}

		public void run() {
			try {
				// This is a blocking call and will only return on a
				// successful connection or an exception
				driver.open();
			} catch (IOException e) {
				// Close the socket
				try {
					driver.close();
				} catch (IOException e2) {
					Log.e(TAG, "unable to close() USB driver during connection failure", e2);
				}
				connectionFailed();
				return;
			}

			// Reset the ConnectThread because we're done
			synchronized (UsbService.this) {
				connectThread = null;
			}

			// Start the connected thread
			connected(driver);
		}

		public void cancel() {
			try {
				driver.close();
				driver = null;
			} catch (IOException e) {
				Log.e(TAG, "close() of connect USB failed", e);
			}
		}
	}

	/**
	 * This thread runs during a connection with a remote device. It handles all
	 * incoming and outgoing transmissions.
	 */
	private class ConnectedThread extends Thread implements IReadoutCallback {
		private final ReadoutParser parser;
		private UsbSerialDriver driver;

		public ConnectedThread(UsbSerialDriver driver, int baudRate) {
			parser = new ReadoutParser(this);
			this.driver = driver;

			try {
				driver.setBaudRate(baudRate);

			} catch (Throwable e) {
				Log.e(TAG, "USB setBaudRate failed", e);
			}
		}

		public void run() {

			byte[] readChar = new byte[2024];
			int offset = 0;

			// Keep listening to the InputStream while connected
			while (driver!=null) {
				try {

					offset = driver.read(readChar, UsbSerialDriver.DEFAULT_READ_BUFFER_SIZE);
					
					// if something was read, send it to the state machine
					for(int i = 0; i<offset; i++)
					{
						// parser will process the input and [if appropriate]
						// call the listener's callback method
						parser.Accept((char) readChar[i]);
					}

				} catch (IOException e) {
					Log.e(TAG, "disconnected", e);
					connectionLost();
					break;
				}
			}
		}

		public void cancel() {
			try {
				driver.close();
				driver = null;
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed", e);
			}
		}

		@Override
		public void processReadout(int axis, int position) {
			handler.obtainMessage(ServiceMessage.POSITION_READ, axis, position, 0).sendToTarget();
		}
	}
}
