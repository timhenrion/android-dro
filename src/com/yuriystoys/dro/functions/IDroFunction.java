package com.yuriystoys.dro.functions;

import android.app.Activity;
import android.view.ViewGroup;

import com.yuriystoys.dro.MachineTypes;

public abstract class IDroFunction {
	
	protected MachineTypes type;
	protected Activity activity;
	
	public MachineTypes getType() {
		return type;
	}
	
	protected IDroFunction(Activity activity, ViewGroup target, MachineTypes machineType)
	{
		this.activity = activity;
		this.type = machineType;
	}
	
	public abstract void showForMachine(MachineTypes type);
}
