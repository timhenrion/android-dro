/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro;

import java.util.ArrayList;
import java.util.List;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.preference.PreferenceManager;

import com.yuriystoys.dro.callbacks.IWorkspaceChanged;
import com.yuriystoys.dro.core.BluetoothDroDriver;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.core.DroDriver;
import com.yuriystoys.dro.core.UsbDroDriver;
import com.yuriystoys.dro.core.Workspace;
import com.yuriystoys.dro.settings.PreferenceKeys;

public class DroApplication extends Application {

	public final static int BAUD_RATE = 9600;

	private static DroApplication currentInstance;

	private final Dro dro;
	private boolean useUsb = false;
	private int usbBaudRate = BAUD_RATE;

	// driver and workspace can change
	private DroDriver driver;
	private Workspace currentWorkspace;

	private final List<IWorkspaceChanged> workspaceChanvedCallbacks = new ArrayList<IWorkspaceChanged>();

	public void registerCallback(IWorkspaceChanged callback) {
		if (!workspaceChanvedCallbacks.contains(callback)) {
			workspaceChanvedCallbacks.add(callback);
		} else {
			throw new IllegalStateException("This callback is already registered.");
		}
	}

	// Methods to remove callback

	public void removeCallback(IWorkspaceChanged callback) {
		if (workspaceChanvedCallbacks.contains(callback)) {
			workspaceChanvedCallbacks.remove(callback);
		} else
			throw new IllegalStateException("Attempting to remove a callback that is not registered");
	}

	public DroApplication() {
		dro = new Dro();
	}

	@Override
	public void onCreate() {
		super.onCreate();

		DroApplication.currentInstance = this;

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

		this.loadPreferences(preferences);

		dro.setMetricMode(preferences.getBoolean(PreferenceKeys.USE_METRIC_MODE, false));
	}
	
	public static boolean isTablet(Context context) {
	    return context.getResources().getConfiguration().smallestScreenWidthDp >= 600;
	}

	public void loadPreferences(SharedPreferences preferences) {
		dro.loadPreferences(preferences, this);

		useUsb = preferences.getBoolean(PreferenceKeys.USE_USB, false);

		try {
			usbBaudRate = preferences.getInt(PreferenceKeys.USB_BAUD_RATE, BAUD_RATE);
		} catch (NumberFormatException ex) {
			usbBaudRate = BAUD_RATE;
		} catch (ClassCastException ex) {
			usbBaudRate = BAUD_RATE;
		}

		// TODO: Make sure the current driver gets disconnected and disposed
		// properly
		if (useUsb) {

			if (driver == null || !(driver instanceof UsbDroDriver))
				driver = new UsbDroDriver((Context) this, usbBaudRate);
		} else {
			if (driver == null || !(driver instanceof BluetoothDroDriver))
				driver = new BluetoothDroDriver((Context) this);
		}
	}

	@Override
	public void onTerminate() {

		super.onTerminate();
	}

	public boolean usesUsb() {
		return useUsb;
	}

	public DroDriver getDriver() {
		return driver;
	}

	public Dro getDro() {
		return dro;
	}

	public Workspace getCurrentWorkspace() {
		return currentWorkspace;
	}

	public void setCurrentWorkspace(Workspace workspace) {
		currentWorkspace = workspace;

		for (IWorkspaceChanged callback : workspaceChanvedCallbacks) {
			callback.onWorkspaceChanged(currentWorkspace);
		}
	}

	public static DroApplication getCurrentInstance() {
		return currentInstance;
	}
}
