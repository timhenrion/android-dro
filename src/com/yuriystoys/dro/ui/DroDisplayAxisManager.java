package com.yuriystoys.dro.ui;

import com.yuriystoys.dro.axes.AxisSettings;

public abstract class DroDisplayAxisManager {
	
	public abstract boolean bind();

	public abstract void unbind();

	public abstract void hide();

	public abstract void show();

	public abstract void setDisabledState();

	public abstract void setEnabledState();

	public abstract void setLcdTypeface(int fontSize);

	public abstract void setControlSize(int fontSize);

	public  abstract int getTargetAxis();

	public  abstract AxisSettings getAxis();
}
