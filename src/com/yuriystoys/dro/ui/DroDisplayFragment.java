/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.ui;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.callbacks.IConnectionStateChangedCallback;
import com.yuriystoys.dro.callbacks.IGlobalModeChangedCallback;
import com.yuriystoys.dro.callbacks.IGlobalUnitsChangedCallback;
import com.yuriystoys.dro.core.ConnectionState;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.core.DroDriver;
import com.yuriystoys.dro.functions.IDroFunction;
import com.yuriystoys.dro.functions.LatheRadiusDiameter;
import com.yuriystoys.dro.functions.LatheToolOffset;
import com.yuriystoys.dro.functions.MillHoleCircle;
import com.yuriystoys.dro.functions.MillHolePattern;
import com.yuriystoys.dro.functions.MillToolOffset;

public class DroDisplayFragment extends Fragment implements IGlobalModeChangedCallback, IGlobalUnitsChangedCallback,
		IConnectionStateChangedCallback {

	private ImageButton unitsButton;
	private ImageButton zeroButton;
	private ImageButton modeButton;

	// indicator label pointers
	// private RelativeLayout[] indicators = new
	// RelativeLayout[DroDriver.SUPPORTED_AXES];
	// private TextView[] inchIndicators = new TextView[Dro.SUPPORTED_AXES];
	// private TextView[] mmIndicators = new TextView[Dro.SUPPORTED_AXES];

	private final ArrayList<IDroFunction> functions = new ArrayList<IDroFunction>();

	private final List<DroDisplayAxisManager> axes = new ArrayList<DroDisplayAxisManager>();

	/**
	 * Caches references to the controls this way they won't need to be looked
	 * up every time we need to do something to one of them
	 */
	private void cacheReferences() {

		unitsButton = (ImageButton) getView().findViewById(R.id.unitsButton);
		zeroButton = (ImageButton) getView().findViewById(R.id.zeroSetButton);

		if (getView().findViewById(R.id.modeSetButton) != null)
			modeButton = (ImageButton) getView().findViewById(R.id.modeSetButton);
		
		if (getView().findViewById(R.id.unitsButton) != null)
			unitsButton = (ImageButton) getView().findViewById(R.id.unitsButton);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		return inflater.inflate(R.layout._fragment_dro_display, container, false);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		cacheReferences();
		initializeAxisManagers();
		initializeFunctions();
		setEventListeners();
	}

	public DroDisplayFragment() {

	}

	@Override
	public void onResume() {

		super.onResume();

		DroApplication app = (DroApplication) getActivity().getApplication();
		
		Dro dro = app.getDro();
		
		intializeUi();

		
		for(DroDisplayAxisManager axis : axes)
		{			
			if(dro.getAxis(axis.getTargetAxis()).isEnabled())
			{
				axis.show();
				axis.bind();
				Log.d("Axis", "Showing " + Integer.toString(axis.getTargetAxis()));
			}
			else
			{
				axis.hide();
				Log.d("Axis", "Hiding " + Integer.toString(axis.getTargetAxis()));
			}
		}
		
		DroDriver driver = app.getDriver();
		
		driver.registerCallback((IConnectionStateChangedCallback) this);
		dro.registerCallback((IGlobalUnitsChangedCallback) this);
		dro.registerCallback((IGlobalModeChangedCallback) this);

		onConnectionStateChanged(driver.getState());
		onModeChanged(dro);
		onUnitsChanged(dro);
		
		/*
		 * // SharedPreferences prefs = //
		 * PreferenceManager.getDefaultSharedPreferences(getActivity()); //
		 * metricMode = prefs.getBoolean(PreferenceKeys.METRIC_MODE, false);
		 * 
		 * // set non-RPM axes to proper modes for (int axis = 0; axis <
		 * Dro.SUPPORTED_AXES; axis++) { onModeChanged(dro); }
		 * 
		 * onUnitsChanged(dro); onConnectionStateChanged(driver.getState()); }
		 */
	}

	@Override
	public void onPause() {

		DroApplication app = (DroApplication) getActivity().getApplication();

		app.getDriver().removeCallback((IConnectionStateChangedCallback) this);

		Dro readout = app.getDro();
		
		for(DroDisplayAxisManager manager : axes)
		{
			manager.unbind();
		}

		readout.removeCallback((IGlobalUnitsChangedCallback) this);
		readout.removeCallback((IGlobalModeChangedCallback) this);
		super.onPause();

	}

	private void initializeAxisManagers() {
		Dro dro = ((DroApplication) getActivity().getApplication()).getDro();

		TextView labelTextView;
		Button readoutTextButton;
		TextView maskTextView;
		ImageButton modeImageButton;
		FrameLayout axisWrapper;
		DroDisplayLinearAxisManager manager;

		RelativeLayout unitsIndicatorWrapper;
		TextView inchTextView;
		TextView mmTextView;

		// x axis

		labelTextView = (TextView) getView().findViewById(R.id.xAxisLabel);
		readoutTextButton = (Button) getView().findViewById(R.id.xTextButton);
		maskTextView = (TextView) getView().findViewById(R.id.xBgTextView);
		modeImageButton = (ImageButton) getView().findViewById(R.id.xModeButton);
		axisWrapper = (FrameLayout) getView().findViewById(R.id.xReadout);

		unitsIndicatorWrapper = (RelativeLayout) (getView().findViewById(R.id.xIndicators) != null ? getView()
				.findViewById(R.id.xIndicators) : null);
		inchTextView = (TextView) (getView().findViewById(R.id.xInchTextView) != null ? getView().findViewById(
				R.id.xInchTextView) : null);
		mmTextView = (TextView) (getView().findViewById(R.id.xMmTextView) != null ? getView().findViewById(
				R.id.xMmTextView) : null);

		manager = new DroDisplayLinearAxisManager(this.getActivity(), dro, Axis.X, labelTextView, axisWrapper,
				readoutTextButton, maskTextView, modeImageButton, unitsIndicatorWrapper, inchTextView, mmTextView);
		manager.hide();

		axes.add(manager);

		// y axis

		labelTextView = (TextView) getView().findViewById(R.id.yAxisLabel);
		readoutTextButton = (Button) getView().findViewById(R.id.yTextButton);
		maskTextView = (TextView) getView().findViewById(R.id.yBgTextView);
		modeImageButton = (ImageButton) getView().findViewById(R.id.yModeButton);
		axisWrapper = (FrameLayout) getView().findViewById(R.id.yReadout);

		unitsIndicatorWrapper = (RelativeLayout) (getView().findViewById(R.id.yIndicators) != null ? getView()
				.findViewById(R.id.yIndicators) : null);
		inchTextView = (TextView) (getView().findViewById(R.id.yInchTextView) != null ? getView().findViewById(
				R.id.yInchTextView) : null);
		mmTextView = (TextView) (getView().findViewById(R.id.yMmTextView) != null ? getView().findViewById(
				R.id.yMmTextView) : null);

		manager = new DroDisplayLinearAxisManager(this.getActivity(), dro,Axis.Y, labelTextView, axisWrapper,
				readoutTextButton, maskTextView, modeImageButton, unitsIndicatorWrapper, inchTextView, mmTextView);
		manager.hide();

		axes.add(manager);

		// z axis

		labelTextView = (TextView) getView().findViewById(R.id.zAxisLabel);
		readoutTextButton = (Button) getView().findViewById(R.id.zTextButton);
		maskTextView = (TextView) getView().findViewById(R.id.zBgTextView);
		modeImageButton = (ImageButton) getView().findViewById(R.id.zModeButton);
		axisWrapper = (FrameLayout) getView().findViewById(R.id.zReadout);

		unitsIndicatorWrapper = (RelativeLayout) (getView().findViewById(R.id.zIndicators) != null ? getView()
				.findViewById(R.id.zIndicators) : null);
		inchTextView = (TextView) (getView().findViewById(R.id.zInchTextView) != null ? getView().findViewById(
				R.id.zInchTextView) : null);
		mmTextView = (TextView) (getView().findViewById(R.id.zMmTextView) != null ? getView().findViewById(
				R.id.zMmTextView) : null);

		manager = new DroDisplayLinearAxisManager(this.getActivity(), dro,Axis.Z, labelTextView, axisWrapper,
				readoutTextButton, maskTextView, modeImageButton, unitsIndicatorWrapper, inchTextView, mmTextView);
		manager.hide();

		axes.add(manager);

		// w axis

		labelTextView = (TextView) getView().findViewById(R.id.wAxisLabel);
		readoutTextButton = (Button) getView().findViewById(R.id.wTextButton);
		maskTextView = (TextView) getView().findViewById(R.id.wBgTextView);
		modeImageButton = (ImageButton) getView().findViewById(R.id.wModeButton);
		axisWrapper = (FrameLayout) getView().findViewById(R.id.wReadout);

		unitsIndicatorWrapper = (RelativeLayout) (getView().findViewById(R.id.wIndicators) != null ? getView()
				.findViewById(R.id.wIndicators) : null);
		inchTextView = (TextView) (getView().findViewById(R.id.wInchTextView) != null ? getView().findViewById(
				R.id.wInchTextView) : null);
		mmTextView = (TextView) (getView().findViewById(R.id.wMmTextView) != null ? getView().findViewById(
				R.id.wMmTextView) : null);

		manager = new DroDisplayLinearAxisManager(this.getActivity(), dro,Axis.W, labelTextView, axisWrapper,
				readoutTextButton, maskTextView, modeImageButton, unitsIndicatorWrapper, inchTextView, mmTextView);
		manager.hide();

		axes.add(manager);

		// tachometer

		labelTextView = (TextView) getView().findViewById(R.id.tachAxisLabel);
		readoutTextButton = (Button) getView().findViewById(R.id.tachTextButton);
		maskTextView = (TextView) getView().findViewById(R.id.tachBgTextView);
		axisWrapper = (FrameLayout) getView().findViewById(R.id.tachReadout);

		DroDisplayTachometerManager tManager = new DroDisplayTachometerManager(this.getActivity(), dro,Axis.T, labelTextView,
				axisWrapper, readoutTextButton, maskTextView);
		
		tManager.hide();

		axes.add(tManager);
	}

	private int getContainerHeight() {
		Context context = getActivity();
		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

		// check display size to figure out what image resolution will be loaded
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		Point size = new Point();
		display.getSize(size);
		// int width = size.x;
		int height = size.y;

		TypedValue tv = new TypedValue();
		int actionBarHeight = 0;
		if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
			actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
		}

		height -= actionBarHeight;

		return height;
	}

	/* UI Initialization */
	private void intializeUi() {

		final Dro dro = ((DroApplication) getActivity().getApplication()).getDro();

		if (modeButton != null) {
			modeButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (nextIsAbsolute) {

						for (int axis = 0; axis < Dro.SUPPORTED_AXES; axis++) {
							dro.getAxis(axis).setAbsoluteMode();
						}

						nextIsAbsolute = false;
					} else {

						for (int axis = 0; axis < Dro.SUPPORTED_AXES; axis++) {
							dro.getAxis(axis).setIncrementalMode();
						}

						nextIsAbsolute = true;
					}
				}
			});
		}

		// calculate number of visible axes and show ones that aren't enabled

		/*
		 * SharedPreferences prefs =
		 * PreferenceManager.getDefaultSharedPreferences(this.getActivity()
		 * .getApplicationContext());
		 */

		View scroll = getView().findViewById(R.id.functionButtons);

		int axisCount = dro.getAxes().size();

		if (scroll != null && scroll instanceof HorizontalScrollView) {
			// resize only if the view is horizontal (in which case it takes up
			// one axis space
			axisCount++; // horizontal scroll is taking up vertical space...
		}

		int contHeight = (int) (getContainerHeight()
				- (getActivity().getResources().getDimension(R.dimen.margin_small) * axisCount) - 20);

		int fontSize = (int) Math.min(contHeight / axisCount,
				getActivity().getResources().getDimension(R.dimen.font_lcd) - 20);

		int buttonSize = (int) Math.min(fontSize - 6, getActivity().getResources().getDimension(R.dimen.dro_button_size));

		// set all relevant controls to proper height
		
		for(DroDisplayAxisManager axis : this.axes)
		{
			axis.setLcdTypeface(fontSize);
			axis.setControlSize(fontSize);
		}

		// resize only if the view is horizontal
		if (scroll != null && scroll instanceof HorizontalScrollView) {
			scroll.getLayoutParams().height = fontSize;
			scroll = ((ViewGroup) scroll).getChildAt(0);

			if (scroll != null && scroll instanceof ViewGroup) {

				for (int i = 0; i < ((ViewGroup) scroll).getChildCount(); ++i) {
					View nextChild = ((ViewGroup) scroll).getChildAt(i);

					if (nextChild != null && nextChild instanceof ImageButton) {
						((ImageButton) nextChild).getLayoutParams().height = buttonSize;
						((ImageButton) nextChild).getLayoutParams().width = buttonSize;
					}
				}
			}

			View addPointButton = getView().findViewById(R.id.add_point_button);

			if (addPointButton != null) {
				((ImageButton) addPointButton).setMaxHeight(buttonSize);
			}
		} else if (scroll != null && scroll instanceof ScrollView) {
			// scroll.getLayoutParams()
		}

		// setLcdTypeface(fontSize);
		setMachineUi();
	}

	private void setMachineUi() {

		for (IDroFunction function : functions)
			function.showForMachine(DroApplication.getCurrentInstance().getDro().getMachineType());
	}


	private void initializeFunctions() {
		ViewGroup functionWrapper = (ViewGroup) getView().findViewById(R.id.function_container);
		if (functionWrapper == null) {
			Toast.makeText(getActivity(), "ERROR: Function button container not defined", Toast.LENGTH_LONG).show();
			return;
		}

		functions.add(new MillHoleCircle(getActivity(), functionWrapper));
		functions.add(new MillHolePattern(getActivity(), functionWrapper));
		functions.add(new MillToolOffset(getActivity(), functionWrapper));
		functions.add(new LatheToolOffset(getActivity(), functionWrapper));
		functions.add(new LatheRadiusDiameter(getActivity(), functionWrapper));

		/*
		 * View holePatternButton =
		 * getView().findViewById(R.id.holePatternButton);
		 * 
		 * if (holePatternButton != null) {
		 * holePatternButton.setOnClickListener(new View.OnClickListener() {
		 * public void onClick(View v) {
		 * HolePatternDialog.Show(DroDisplayFragment.this.getActivity()); } });
		 * }
		 * 
		 * View toolOffsetButton =
		 * getView().findViewById(R.id.indicateEdgeButton);
		 * 
		 * if (holePatternButton != null) {
		 * toolOffsetButton.setOnClickListener(new View.OnClickListener() {
		 * public void onClick(View v) {
		 * ToolOffsetDialog.Show(DroDisplayFragment.this.getActivity()); } }); }
		 */
	}

	private void setEventListeners() {
		/*
		 * axisText[axis].setOnLongClickListener(new OnLongClickListener() {
		 * 
		 * @Override public boolean onLongClick(View v) { zeroSet(axis); return
		 * true; } });
		 * 
		 * findViewById(R.id.xWrapper).setOnTouchListener( new
		 * axisOnSwipeTouchListener(this, Axis.X));
		 * 
		 * axisText[Axis.Z].setOnTouchListener(new
		 * axisOnSwipeTouchListener(this, Axis.Z));
		 */

		final Dro dro = ((DroApplication) getActivity().getApplication()).getDro();

		if (unitsButton != null) {
			unitsButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dro.toggleMetricMode();
				}
			});
		}

		if (zeroButton != null) {
			zeroButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dro.setZero();
					((DroApplication)getActivity().getApplication()).getCurrentWorkspace().clearSelectedPointId();
				}
			});

			zeroButton.setOnLongClickListener(new OnLongClickListener() {

				@SuppressLint("ValidFragment") @Override
				public boolean onLongClick(View v) {

					DialogFragment dialog = new DialogFragment() {

						@Override
						public Dialog onCreateDialog(Bundle savedInstanceState) {
							AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

							builder.setTitle(R.string.origin_dialog_title).setMessage(R.string.origin_dialog_message)
									.setPositiveButton(R.string.yes_button, new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											dro.setOrigin();
										}
									}).setNegativeButton(R.string.no_button, null);
							// Create the AlertDialog object and return it
							return builder.create();
						}

					};
					dialog.show(getActivity().getSupportFragmentManager(), "Dialog");

					return true;
				}
			});
		}

		View addPointButton = getView().findViewById(R.id.add_point_button);

		if (addPointButton != null) {
			addPointButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					DroApplication app = ((DroApplication) getActivity().getApplication());
					Dro dro = app.getDro();

					app.getCurrentWorkspace().addPoint(dro.getCurrentPoint());
				}
			});

			addPointButton.setOnLongClickListener(new OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {

					DroApplication app = ((DroApplication) getActivity().getApplication());
					Dro dro = app.getDro();

					PointDetailsDialog.AddPoint(getActivity(), dro.getCurrentPoint());
					return true;
				}
			});
		}

	}

	// modes go: mixed->all in relative->all in absolute->all in relative->all
	// in absolute and so on
	boolean nextIsAbsolute = false;

	@Override
	public void onModeChanged(Dro sender) {

		if (modeButton != null) {
			Resources res = getResources();

			if (sender.allAxesInAbsoluteMode()) {
				// we're in absolute mode
				nextIsAbsolute = false;
				modeButton.setImageDrawable(res.getDrawable(R.drawable.button_abs));
			} else if (sender.allAxesInIncrementalMode()) {
				// we're in relative mode
				nextIsAbsolute = true;
				modeButton.setImageDrawable(res.getDrawable(R.drawable.button_incr));
			} else {
				// we're in mixed mode
				nextIsAbsolute = true;
				modeButton.setImageDrawable(res.getDrawable(R.drawable.button_mode));
			}
		}
	}

	@Override
	public void onUnitsChanged(Dro sender) {

		if (unitsButton != null) {
			if (sender.isInMetricMode()) {
				unitsButton.setImageDrawable(getResources().getDrawable(R.drawable.button_mm));
			} else {
				unitsButton.setImageDrawable(getResources().getDrawable(R.drawable.button_inch));
			}
		}
	}

	@Override
	public void onConnectionStateChanged(int status) {
		if (status == ConnectionState.CONNECTED) {

			for (DroDisplayAxisManager axis : axes) {
				axis.setEnabledState();
			}
		} else {
			for (DroDisplayAxisManager axis : axes) {
				axis.setDisabledState();
			}
		}
	}

}
