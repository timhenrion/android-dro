package com.yuriystoys.dro.ui.util;

import java.util.Timer;
import java.util.TimerTask;

import android.media.AudioManager;
import android.media.ToneGenerator;
import android.util.Log;

import com.yuriystoys.dro.axes.AxisSettings;
import com.yuriystoys.dro.callbacks.IPositionChangedCallback;

public class NearZeroWarning implements IPositionChangedCallback {

	private final ToneGenerator tg;
	private boolean started;
	private final AxisSettings axis;
	private Timer timer;
	
	private final int tesholdL1;
	private final int tesholdL2;
	private final int tesholdL3;
	private final int tesholdL4;
	private final int tesholdL5;

	private final int NO_REPEAT = -1;

	private WarningStates lastState = WarningStates.None;

	private enum WarningStates {
		L1, L2, L3, L4, L5, None
	}

	public NearZeroWarning(AxisSettings axis) {
		tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, ToneGenerator.MAX_VOLUME);
		this.axis = axis;

		// TODO: Add a setting for the tresholds

		if (axis.isInMetricMode()) {
			this.tesholdL1 = axis.convertMmToCounts(0.5);
			this.tesholdL2 = axis.convertMmToCounts(.1);
			this.tesholdL3 = axis.convertMmToCounts(2.5);
			this.tesholdL4 = axis.convertMmToCounts(5);
			this.tesholdL5 = axis.convertMmToCounts(10);
		} else {
			this.tesholdL1 = axis.convertInchesToCounts(0.001);
			this.tesholdL2 = axis.convertInchesToCounts(0.005);
			this.tesholdL3 = axis.convertInchesToCounts(0.01);
			this.tesholdL4 = axis.convertInchesToCounts(0.1);
			this.tesholdL5 = axis.convertInchesToCounts(0.25);
		}
	}

	public void release() {
		if (this.timer != null) {
			timer.cancel();
			timer.purge();
		}
		tg.stopTone();
		tg.release();
	}

	public void start() {
		if (!started) {
			lastState = WarningStates.None;
			axis.registerCallback(this);
			started = true;
		}
	}

	public void stop() {
		if (started) {
			axis.removeCallback(this);
			started = false;
			stopWarning();
			if (this.timer != null) {
				timer.cancel();
				timer.purge();
			}
			tg.stopTone();
		}
	}

	private void startL1Warning() {
		if (lastState != WarningStates.L1) {
			startBeep(ToneGenerator.TONE_DTMF_0, 2500, NO_REPEAT);
			lastState = WarningStates.L1;
		}
	}

	private void startL2Warning() {
		if (lastState != WarningStates.L2) {
			startBeep(ToneGenerator.TONE_DTMF_0, 100, 100);
			lastState = WarningStates.L2;
		}
	}

	private void startL3Warning() {
		if (lastState != WarningStates.L3) {
			startBeep(ToneGenerator.TONE_DTMF_0, 100, 200);
			lastState = WarningStates.L3;
		}
	}

	private void startL4Warning() {
		if (lastState != WarningStates.L4) {
			startBeep(ToneGenerator.TONE_DTMF_0, 100, 400);
			lastState = WarningStates.L4;
		}
	}

	private void startL5Warning() {
		if (lastState != WarningStates.L5) {
			startBeep(ToneGenerator.TONE_DTMF_0, 100, 800);
			lastState = WarningStates.L5;
		}
	}

	private void stopWarning() {
		if (lastState != WarningStates.None) {

			lastState = WarningStates.None;
			if (this.timer != null) {
				timer.cancel();
				timer.purge();
			}
			tg.stopTone();
		}
	}

	/**
	 * 
	 * @param toneType
	 *            ToneGenerator tone type
	 * @param duration
	 *            beep duration in ms
	 * @param interval
	 *            beep interval in ms
	 */
	private void startBeep(final int toneType, final int duration, int interval) {
		if (this.timer != null) {
			timer.cancel();
			timer.purge();
		}

		if (interval != NO_REPEAT) {
			this.timer = new Timer();

			timer.schedule(new TimerTask() {

				@Override
				public void run() {
					Log.d("Warning", "Beep!");
					tg.stopTone();
					tg.startTone(toneType, duration);
				}
			}, 0, interval); // subsequent rate
		} else {
			tg.stopTone();
			tg.startTone(toneType, duration);
		}
	}

	@Override
	public void onPositionChanged(AxisSettings sender) {
		int position = Math.abs(sender.getPosition());

		if (position <= tesholdL1) {
			startL1Warning();
		} else if (position <= tesholdL2) {
			startL2Warning();
		} else if (position <= tesholdL3) {
			startL3Warning();
		} else if (position <= tesholdL4) {
			startL4Warning();
		} else if (position <= tesholdL5) {
			startL5Warning();
		} else {
			stopWarning();
		}
	}
}
