/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.ui;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.callbacks.IGlobalModeChangedCallback;
import com.yuriystoys.dro.callbacks.IGlobalUnitsChangedCallback;
import com.yuriystoys.dro.callbacks.ISelectedToolChanged;
import com.yuriystoys.dro.callbacks.IToolListChangedCallback;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.data.Repository;
import com.yuriystoys.dro.ui.util.ToolsAdapter;
import com.yuriystoys.dro.ui.util.ToolsAdapter.ToolViewModel;

public class ToolListFragment extends Fragment implements IGlobalUnitsChangedCallback, IGlobalModeChangedCallback,
		IToolListChangedCallback, ISelectedToolChanged {
	// Workspace stuff
	private ToolsAdapter toolsAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout._fragment_tool_list, container, false);
	}

	@Override
	public void onPause() {
		toolsAdapter = null;

		DroApplication app = (DroApplication) getActivity().getApplication();

		app.getDro().removeCallback((IGlobalModeChangedCallback) this);
		app.getDro().removeCallback((IGlobalUnitsChangedCallback) this);
		app.getDro().removeCallback((ISelectedToolChanged) this);
		ToolsAdapter.removeCallback((IToolListChangedCallback) this);

		super.onPause();
	}

	@Override
	public void onResume() {

		super.onResume();

		DroApplication app = ((DroApplication) getActivity().getApplication());

		app.getDro().registerCallback((IGlobalModeChangedCallback) this);
		app.getDro().registerCallback((IGlobalUnitsChangedCallback) this);
		app.getDro().registerCallback((ISelectedToolChanged) this);

		ToolsAdapter.registerCallback((IToolListChangedCallback) this);

		initToolsList();

		toolsAdapter.refresh();

	}

	public void initToolsList() {
		final ListView toolsListView = (ListView) getView().findViewById(R.id.tool_list_view);

		toolsAdapter = new ToolsAdapter(getActivity(), ((DroApplication) getActivity().getApplication()).getDro(),
				R.layout.list_item_point, new ArrayList<ToolViewModel>());

		// long click set the current point as relative originShape for the
		// workspace
		toolsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View arg1, int position, long id) {
				ToolViewModel toolView = (ToolViewModel) adapter.getItemAtPosition(position);

				toolsAdapter.refresh();

				Dro dro = DroApplication.getCurrentInstance().getDro();

				switch (dro.getMachineType()) {
				case VERTICAL_MILL: {
					ToolOffsetDialog.Show(getActivity(), toolView.id);
					break;
				}
				case LATHE: {
					double offsetX = dro.isInMetricMode() ? toolView.offsetX * Dro.MM_PER_INCH : toolView.offsetX;
					double offsetZ = dro.isInMetricMode() ? toolView.offsetZ * Dro.MM_PER_INCH : toolView.offsetZ;

					dro.getAxis(Axis.X).setToolOffset(-offsetX);
					dro.getAxis(Axis.Z).setToolOffset(-offsetZ);

					dro.setSelectedToolId(toolView.id);
					break;
				}
				case HORIZONTAL_MILL:
				{
					break;
				}
				}

				// switch to the DRO screen (when in phone mode)
				((MainActivity) getActivity()).goToReadoutScreen();

			}
		});

		// short click opens point details
		toolsListView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View arg1, int position, long id) {

				final ToolViewModel toolView = (ToolViewModel) adapter.getItemAtPosition(position);

				// outer dialog
				if (toolView != null) {

					View view = getActivity().getLayoutInflater().inflate(R.layout._fragment_tool_commands, null);

					// create tools dialog

					Builder toolDialogBuilder = new Builder(getActivity()).setTitle(R.string.tool_commands_title)
							.setView(view)
							.setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {

								}
							});

					final AlertDialog toolDialog = toolDialogBuilder.create();

					View temp;

					temp = view.findViewById(R.id.addToolButton);
					if (temp != null && temp instanceof Button) {

						((Button) temp).setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								toolDialog.dismiss();

								switch (DroApplication.getCurrentInstance().getDro().getMachineType()) {
								case LATHE: {
									LatheToolEditDialog.Show(getActivity());
									break;
								}
								default: {

								}
									MillToolEditDialog.Show(getActivity());
								}
							}
						});
					}

					temp = view.findViewById(R.id.editToolButton);
					if (temp != null && temp instanceof Button) {

						((Button) temp).setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								toolDialog.dismiss();

								switch (DroApplication.getCurrentInstance().getDro().getMachineType()) {
								case LATHE: {
									LatheToolEditDialog.Show(getActivity(), toolView.id);
									break;
								}
								default: {

								}
									MillToolEditDialog.Show(getActivity(), toolView.id);
								}

							}
						});
					}

					temp = view.findViewById(R.id.deleteToolButton);
					if (temp != null && temp instanceof Button) {

						((Button) temp).setOnClickListener(new OnClickListener() {

							@SuppressLint("ValidFragment") @Override
							public void onClick(View v) {
								toolDialog.dismiss();

								DialogFragment dialog = new DialogFragment() {

									@Override
									public Dialog onCreateDialog(Bundle savedInstanceState) {
										AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

										builder.setTitle(R.string.tool_commands_delete_title)
												.setMessage(R.string.tool_commands_delete_message)
												.setPositiveButton(R.string.yes_button,
														new DialogInterface.OnClickListener() {
															@Override
															public void onClick(DialogInterface dialog, int which) {

																Repository repo = Repository.open(getActivity());
																repo.deleteTool(toolView.id);
																toolsAdapter.refresh();
															}
														}).setNegativeButton(R.string.no_button, null);
										// Create the AlertDialog object and
										// return it
										return builder.create();
									}

								};
								dialog.show(getActivity().getSupportFragmentManager(), "Dialog");

							}
						});
					}

					toolDialog.show();

					return true;
				}
				return false;
			}
		});

		toolsListView.setAdapter(toolsAdapter);

	}

	@Override
	public void onModeChanged(Dro sender) {
		toolsAdapter.notifyDataSetChanged();
	}

	@Override
	public void onUnitsChanged(Dro sender) {
		toolsAdapter.notifyDataSetChanged();
	}

	@Override
	public void onToolListChangedChanged() {
		toolsAdapter.refresh();
	}

	@Override
	public void onSelectedToolChanged() {
		toolsAdapter.refresh();
	}
}
