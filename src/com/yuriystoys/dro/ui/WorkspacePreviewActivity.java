package com.yuriystoys.dro.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.axes.AxisSettings;
import com.yuriystoys.dro.callbacks.IConnectionStateChangedCallback;
import com.yuriystoys.dro.callbacks.IPositionChangedCallback;
import com.yuriystoys.dro.callbacks.IReadoutCallback;
import com.yuriystoys.dro.callbacks.IToastCallback;
import com.yuriystoys.dro.core.ConnectionState;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.core.DroDriver;
import com.yuriystoys.dro.core.Workspace;
import com.yuriystoys.dro.data.Repository;
import com.yuriystoys.dro.settings.BundleKeys;

public class WorkspacePreviewActivity extends Activity implements IToastCallback, IConnectionStateChangedCallback,
		IPositionChangedCallback {

	private WorkspaceView canvas;
	private Workspace workspace;
	private int workspaceId = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		workspaceId = intent.getIntExtra(BundleKeys.WORKSPACE_ID, 0);

		try {
			Repository repo = Repository.open(this);
			workspace = repo.getWorkspace(workspaceId);
		} catch (Exception ex) {
			Toast.makeText(this, R.string.reporsitory_failed_to_get_workspace, Toast.LENGTH_LONG).show();
			this.finish();
			return;
		}

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		Dro dro = ((DroApplication) getApplication()).getDro();

		setContentView(R.layout._fragment_workspace_preview);

		canvas = new WorkspaceView(this);
		canvas.setWorkspace(workspace);
		canvas.setOrigin(dro.getAxis(Axis.X).getIncrementalOffset(),dro.getAxis(Axis.Y).getIncrementalOffset());
		canvas.setBackgroundColor(getResources().getColor(android.R.color.background_dark));

		dro.getAxis(Axis.X).registerCallback((IPositionChangedCallback) canvas);
		dro.getAxis(Axis.Y).registerCallback((IPositionChangedCallback) canvas);

		((FrameLayout) this.findViewById(R.id.workspaceViewFrame)).addView(canvas);
	}

	@Override
	protected void onPause() {
		workspace = null;

		DroApplication app = (DroApplication) getApplication();

		// this should succeed, since the parent activity is checking for this
		Dro dro = app.getDro();

		DroDriver driver = app.getDriver();

		driver.removeCallback((IToastCallback) this);
		driver.removeCallback((IConnectionStateChangedCallback) this);

		driver.removeCallback((IReadoutCallback) dro);
		
		dro.getAxis(Axis.X).removeCallback((IPositionChangedCallback) canvas);
		dro.getAxis(Axis.Y).removeCallback((IPositionChangedCallback) canvas);

		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();

		super.onResume();

		DroApplication app = (DroApplication) getApplication();

		DroDriver driver = app.getDriver();

		driver.registerCallback((IToastCallback) this);
		driver.registerCallback((IConnectionStateChangedCallback) this);
		driver.registerCallback((IReadoutCallback) app.getDro());

		this.onConnectionStateChanged(driver.getState());

	}

	public void refresh() {

	}

	@Override
	public void onConnectionStateChanged(int status) {
		ActionBar bar = this.getActionBar();
		int actionBarTitleId = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		TextView actionBarTextView = (TextView) findViewById(actionBarTitleId);

		switch (status) {
		case ConnectionState.CONNECTED:

			bar.setTitle("Preview Workspace - Connected");
			actionBarTextView.setTextColor(getResources().getColor(android.R.color.holo_green_light));

			break;
		case ConnectionState.CONNECTING:

			bar.setTitle("Preview Workspace - Connecting");
			actionBarTextView.setTextColor(getResources().getColor(android.R.color.holo_green_dark));

			break;
		case ConnectionState.CONNECTION_LOST:
			bar.setTitle("Preview Workspace - Connection lost");
			actionBarTextView.setTextColor(getResources().getColor(android.R.color.holo_orange_light));

			break;
		case ConnectionState.DISABLED:
		case ConnectionState.DISCONNECTED:
		default:
			bar.setTitle("Preview Workspace - Disconnected");
			actionBarTextView.setTextColor(getResources().getColor(android.R.color.white));

			break;
		}
	}

	@Override
	public void MakeToast(String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onPositionChanged(AxisSettings sender) {
		// TODO Auto-generated method stub
	}

}
