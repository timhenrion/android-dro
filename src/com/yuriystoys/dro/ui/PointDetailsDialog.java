/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Toast;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.core.Dro.ReadoutFormat;
import com.yuriystoys.dro.core.Point;
import com.yuriystoys.dro.data.Repository;

public class PointDetailsDialog extends DialogFragment {

	private static final String POINT_ID = "point_id";
	private static final String POINT_X = "point_x";
	private static final String POINT_Y = "point_y";
	private static final String POINT_Z = "point_z";

	private int pointId = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		View view = getActivity().getLayoutInflater().inflate(R.layout._fragment_point_details, null);

		setEventListeners(view);

		Bundle bundle = getArguments();

		if (bundle != null) {
			pointId = bundle.getInt(POINT_ID, -1);

			Dro dro = ((DroApplication) getActivity().getApplication()).getDro();
			ReadoutFormat format = dro.getCurrentFormat();
			
			if (pointId > 0) {
				Repository repo = Repository.open(getActivity());

				try {
					Point point = repo.getPoint(pointId);

					if (_nameEdit != null && point.getName() != null)
						_nameEdit.setText(point.getName());

					if (_descriptionEdit != null && point.getDescription() != null)
						_descriptionEdit.setText(point.getDescription());

					if (_xEdit != null)
						_xEdit.setText(format.format(dro.getAxis(Axis.X).convertToDimension(point.getX())));

					if (_yEdit != null)
						_xEdit.setText(format.format(dro.getAxis(Axis.Y).convertToDimension(point.getY())));

					if (point.getZ() != null) {
						if (_zEdit != null) {
							_zEdit.setText(format.format(dro.getAxis(Axis.Z).convertToDimension(point.getZ())));
							_zEdit.setEnabled(true);
						}

						if (_zEnable != null)
							_zEnable.setChecked(true);
					}
				} finally {
					repo.close();
				}
			} else {
				if (bundle.get(POINT_X) != null && _xEdit != null)
					_xEdit.setText(format.format(dro.getAxis(Axis.X).convertToDimension(bundle.getInt(POINT_X))));

				if (bundle.get(POINT_Y) != null && _yEdit != null)
					_yEdit.setText(format.format(dro.getAxis(Axis.Y).convertToDimension(bundle.getInt(POINT_Y))));

				if (bundle.get(POINT_Z) != null && _zEdit != null) {
					_zEdit.setText(format.format(dro.getAxis(Axis.Z).convertToDimension(bundle.getInt(POINT_Z))));

					if (_zEnable != null)
						_zEnable.setChecked(true);
				}

			}
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.point_details_edit_title);
		builder.setCancelable(false);
		builder.setView(view);

		// set dialog message
		builder.setCancelable(false);

		builder.setPositiveButton(R.string.point_details_ok_edit, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// do nothing here. The real handler is below
			}
		});

		builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// if this button is clicked, just close
				// the dialog box and do nothing
				dialog.cancel();
			}
		});

		// create alert dialog
		AlertDialog dialog = builder.create();

		dialog.setOnShowListener(new OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				Window window = ((AlertDialog) dialog).getWindow();

				if (getResources().getConfiguration().screenHeightDp < 400)
					window.setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

				((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
						new View.OnClickListener() {
							@Override
							public void onClick(View v) {

								if (SaveEditedPoint(pointId)) {
									((DroApplication) getActivity().getApplication()).getCurrentWorkspace().refresh();
									dismiss();
								}
							}
						});
			}
		});

		dialog.setCanceledOnTouchOutside(false);

		return dialog;

	}

	private EditText _nameEdit;
	private EditText _descriptionEdit;

	private EditText _xEdit;
	private EditText _yEdit;
	private EditText _zEdit;
	private CheckBox _zEnable;

	private void setEventListeners(View view) {

		View temp;

		temp = view.findViewById(R.id.pointNameEdit);
		if (temp != null && temp instanceof EditText) {
			_nameEdit = (EditText) temp;
		}

		temp = view.findViewById(R.id.pointDescriptionEdit);
		if (temp != null && temp instanceof EditText) {
			_descriptionEdit = (EditText) temp;
		}

		temp = view.findViewById(R.id.xCoordEdit);
		if (temp != null && temp instanceof EditText) {
			_xEdit = (EditText) temp;
		}

		temp = view.findViewById(R.id.yCoordEdit);
		if (temp != null && temp instanceof EditText) {
			_yEdit = (EditText) temp;
		}

		temp = view.findViewById(R.id.zCoordEdit);
		if (temp != null && temp instanceof EditText) {
			_zEdit = (EditText) temp;
		}

		temp = view.findViewById(R.id.zCoordEnableCheckBox);
		if (temp != null && temp instanceof CheckBox) {
			_zEnable = (CheckBox) temp;

			_zEnable.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

					if (isChecked) {
						_zEdit.setEnabled(true);
					} else {
						_zEdit.setEnabled(false);
						_zEdit.setText("");
					}
				}
			});
		}

	}

	private boolean SaveEditedPoint(int pointId) {

		Dro dro = ((DroApplication) getActivity().getApplication()).getDro();

		int x, y;
		Integer z = null;

		String name = _nameEdit != null ? _nameEdit.getText().toString() : null;
		String description = _descriptionEdit != null && _descriptionEdit.getText().toString().trim().length() > 0 ? _descriptionEdit
				.getText().toString() : null;

		try {
			x = dro.getAxis(Axis.X).convertToCounts(Double.parseDouble(_xEdit.getText().toString()));
		} catch (NumberFormatException ex) {
			Toast.makeText(getActivity(), R.string.point_details_x_invalid, Toast.LENGTH_LONG).show();
			return false;
		}

		try {
			y = dro.getAxis(Axis.Y).convertToCounts(Double.parseDouble(_yEdit.getText().toString()));
		} catch (NumberFormatException ex) {
			Toast.makeText(getActivity(), R.string.point_details_y_invalid, Toast.LENGTH_LONG).show();
			return false;
		}

		try {
			if (_zEnable.isChecked())
				z = dro.getAxis(Axis.Z).convertToCounts(Double.parseDouble(_zEdit.getText().toString()));

		} catch (NumberFormatException ex) {
			Toast.makeText(getActivity(), R.string.point_details_z_invalid, Toast.LENGTH_LONG).show();
			return false;
		}

		Repository repo = Repository.open(getActivity());

		try {
			// TODO: This will need to change for the new point (once needed)

			if (pointId > 0) {
				if (name == null | name.trim().length() == 0) {
					Toast.makeText(getActivity(), R.string.point_details_name_required, Toast.LENGTH_LONG).show();
					return false;
				}

				Point point = repo.getPoint(pointId);

				if (z != null) {
					point.updatePoint3D(name, description, x, y, z, null);
				} else {
					point.updatePoint2D(name, description, x, y, null);
				}

				repo.update(point);
			} else {
				Point point;

				if (z != null) {
					point = Point.createPoint3D(name, description, x, y, z, null);
				} else {
					point = Point.createPoint2D(name, description, x, y, null);
				}

				repo.insertPoint(((DroApplication) getActivity().getApplication()).getCurrentWorkspace().getId(), point);
			}

		} catch (Exception ex) {
			Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
			return false;
		}

		return true;
	}

	public static void ShowPoint(Activity activity, Integer pointId) {

		FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
		Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);

		// Create and show the dialog.
		DialogFragment newFragment = new PointDetailsDialog();

		if (pointId != null) {
			Bundle args = new Bundle();
			args.putInt(POINT_ID, pointId);
			newFragment.setArguments(args);
		}
		newFragment.show(ft, "dialog");

	}

	public static void AddPoint(Activity activity, Point point) {

		FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
		Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);

		// Create and show the dialog.
		DialogFragment newFragment = new PointDetailsDialog();

		Bundle args = new Bundle();
		args.putInt(POINT_X, point.getX());
		args.putInt(POINT_Y, point.getY());

		if (point.getZ() != null)
			args.putInt(POINT_Z, point.getZ());
		newFragment.setArguments(args);

		newFragment.show(ft, "dialog");

	}
}
