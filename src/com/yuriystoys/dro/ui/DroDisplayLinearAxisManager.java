package com.yuriystoys.dro.ui;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.MachineTypes;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.axes.AxisSettings;
import com.yuriystoys.dro.callbacks.IModeChangedCallback;
import com.yuriystoys.dro.callbacks.IPositionChangedCallback;
import com.yuriystoys.dro.callbacks.IUnitsChangedCallback;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.settings.PreferenceKeys;

public class DroDisplayLinearAxisManager extends DroDisplayAxisManager implements IPositionChangedCallback,
		IUnitsChangedCallback, IModeChangedCallback {

	private final int targetAxis;
	private final Activity activity;
	private final Dro dro;
	private AxisSettings axis;

	private TextView labelTextView;
	private Button readoutTextButton;
	private TextView maskTextView;
	private ImageButton modeImageButton;
	private FrameLayout axisWrapper;

	private RelativeLayout unitsIndicatorWrapper;
	private TextView inchTextView;
	private TextView mmTextView;

	public DroDisplayLinearAxisManager(Activity activity, Dro dro, int targetAxis, TextView labelTextView,
			FrameLayout axisWrapper, Button readoutTextButton, TextView maskTextView, ImageButton modeImageButton,
			RelativeLayout unitsIndicatorWrapper, TextView inchTextView, TextView mmTextView) {

		this.targetAxis = targetAxis;
		this.activity = activity;
		this.dro = dro;

		this.labelTextView = labelTextView;
		this.readoutTextButton = readoutTextButton;
		this.maskTextView = maskTextView;
		this.modeImageButton = modeImageButton;
		this.axisWrapper = axisWrapper;

		this.unitsIndicatorWrapper = unitsIndicatorWrapper;
		this.inchTextView = inchTextView;
		this.mmTextView = mmTextView;
	}

	@Override
	public boolean bind() {
		AxisSettings axis = dro.getAxis(targetAxis);

		if (axis == null)
			return false;

		if (this.axis != null)
			unbind();

		this.axis = axis;

		axis.registerCallback((IModeChangedCallback) this);
		axis.registerCallback((IUnitsChangedCallback) this);
		axis.registerCallback((IPositionChangedCallback) this);

		labelTextView.setText(axis.getLabel());

		setEventListeners();

		// onPositionChanged(axis);
		onUnitsChanged(axis);

		return true;
	}

	@Override
	public void unbind() {
		// TODO: Remove callbacks
		if (this.axis != null) {
			axis.removeCallback((IModeChangedCallback) this);
			axis.removeCallback((IUnitsChangedCallback) this);
			axis.removeCallback((IPositionChangedCallback) this);
			this.axis = null;
		}
	}

	private void setEventListeners() {

		readoutTextButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//AxisOffsetDialog.show(context, axis.getAxis(), dro, false);
				
				if(dro.getMachineType()== MachineTypes.VERTICAL_MILL)
				{
					LinearAxisDetailsFragment.Show(activity, targetAxis);
				}
				else if(axis.getAxis() == Axis.Z)
				{
					LatheZAxisDetailsFragment.Show(activity, targetAxis);
				}
				else
				{
					LinearAxisToolsDialog.Show(activity, targetAxis);
				}
			}
		});

		readoutTextButton.setLongClickable(true);
		readoutTextButton.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				// setNarrowDisplay();
				LinearAxisToolsDialog.Show(activity, targetAxis);
				return true;
			}
		});

		modeImageButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				axis.toggleMode();
			}
		});

		modeImageButton.setLongClickable(true);
		modeImageButton.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				axis.setIncrementalOrigin();
				DroApplication.getCurrentInstance().getCurrentWorkspace().clearSelectedPointId();
				return true;
			}
		});
	}

	/**
	 * Sets the LCD font on the LCD controls
	 */
	@Override
	public void setLcdTypeface(int fontSize) {

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());

		int displayFont = Integer.parseInt(prefs.getString(PreferenceKeys.DISPLAY_FONT, "0"));

		switch (displayFont) {
		case 0: {
			Typeface font = Typeface.createFromAsset(activity.getAssets(), "fonts/digital-7-italic.ttf");

			readoutTextButton.setTypeface(font);
			readoutTextButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize + 20);
			
			readoutTextButton.setPadding(0, 0, 0, 0);

			maskTextView.setVisibility(View.VISIBLE);
			maskTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize + 20);
			
			maskTextView.setTypeface(font);
			maskTextView.setPadding(0, 0, 0, 0);

			break;
		}
		case 1: {
			Typeface font = Typeface.MONOSPACE;
			
			int lcdSize = (int) activity.getResources().getDimension(R.dimen.font_lcd);
			double monoSize = (double) activity.getResources().getDimension(R.dimen.font_lcd_monospace);
			
			double ratio = monoSize / lcdSize;

			fontSize = (int)(fontSize * ratio);
			
			readoutTextButton.setTypeface(font);
			readoutTextButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
			
			readoutTextButton.setPadding(0, (int) (-fontSize * 0.20), 0, 0);

			maskTextView.setVisibility(View.GONE);
			maskTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);

			break;
		}
		case 2: {
			Typeface font = Typeface.DEFAULT;
			
			int lcdSize = (int) activity.getResources().getDimension(R.dimen.font_lcd);
			double systemSize = (double) activity.getResources().getDimension(R.dimen.font_lcd_system);
			
			double ratio = systemSize / lcdSize;
			
			fontSize = (int)(fontSize * ratio);

			readoutTextButton.setTypeface(font);
			readoutTextButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
			
			readoutTextButton.setPadding(0, (int) (-fontSize * 0.25), 0, 0);

			maskTextView.setVisibility(View.GONE);
			maskTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);

			break;
		}
		}
	}

	@Override
	public void setControlSize(int fontSize) {
		int buttonSize = (int) Math.min(fontSize - 6, activity.getResources().getDimension(R.dimen.dro_button_size));

		
		readoutTextButton.getLayoutParams().height = fontSize;
		
		maskTextView.getLayoutParams().height = fontSize;

		modeImageButton.setMaxHeight(buttonSize);
		modeImageButton.getLayoutParams().height = buttonSize;

		if (unitsIndicatorWrapper != null)
			unitsIndicatorWrapper.getLayoutParams().height = fontSize;
	}

	@Override
	public void setEnabledState() {
		readoutTextButton.setEnabled(true);
	}

	@Override
	public void setDisabledState() {
		readoutTextButton.setEnabled(false);
	}

	@Override
	public void show() {
		labelTextView.setVisibility(View.VISIBLE);
		axisWrapper.setVisibility(View.VISIBLE);
		modeImageButton.setVisibility(View.VISIBLE);

		if (unitsIndicatorWrapper != null)
			unitsIndicatorWrapper.setVisibility(View.VISIBLE);

		if (inchTextView != null)
			inchTextView.setVisibility(View.VISIBLE);

		if (mmTextView != null)
			mmTextView.setVisibility(View.VISIBLE);
	}

	@Override
	public void hide() {
		labelTextView.setVisibility(View.GONE);
		axisWrapper.setVisibility(View.GONE);
		modeImageButton.setVisibility(View.GONE);

		if (unitsIndicatorWrapper != null)
			unitsIndicatorWrapper.setVisibility(View.GONE);

		if (inchTextView != null)
			inchTextView.setVisibility(View.GONE);

		if (mmTextView != null)
			mmTextView.setVisibility(View.GONE);
	}

	/* Callbacks */

	@Override
	public void onPositionChanged(AxisSettings sender) {
		readoutTextButton.setText(dro.getCurrentFormat().format(sender.getReadout()));
	}

	// modes go: mixed->all in relative->all in absolute->all in relative->all
	// in absolute and so on
	boolean nextIsAbsolute = false;

	@Override
	public void onModeChanged(AxisSettings sender) {

		Resources res = activity.getResources();

		if (sender.isInIncrementalMode())
			modeImageButton.setImageDrawable(res.getDrawable(R.drawable.button_incr));
		else
			modeImageButton.setImageDrawable(res.getDrawable(R.drawable.button_abs));

		if (sender.isInIncrementalMode() && sender.hasToolOffset()) {
			readoutTextButton.setTextColor(res.getColorStateList(R.color.lcd_color_warning));
		} else {
			readoutTextButton.setTextColor(res.getColorStateList(R.color.lcd_color));
		}

		onPositionChanged(sender);

		// lcd_color.xml
	}

	@Override
	public void onUnitsChanged(AxisSettings sender) {

		if (inchTextView != null && mmTextView != null) {
			if (sender.isInMetricMode()) {
				inchTextView.setTextColor(activity.getResources().getColor(android.R.color.transparent));
				mmTextView.setTextColor(activity.getResources().getColor(android.R.color.holo_green_light));
			} else {
				inchTextView.setTextColor(activity.getResources().getColor(android.R.color.holo_green_light));
				mmTextView.setTextColor(activity.getResources().getColor(android.R.color.transparent));
			}
		}

		onPositionChanged(sender);
	}

	@Override
	public int getTargetAxis() {
		return targetAxis;
	}

	@Override
	public AxisSettings getAxis() {
		return this.axis;
	}
}
