package com.yuriystoys.dro.axes;

public abstract class AxisReadoutProcessor {

	protected final AxisSettings axis;
	
	public AxisReadoutProcessor(AxisSettings axis)
	{
		this.axis = axis;
	}
	
	public abstract void accept(int position);
}
